Lab: (optional) Mail ~really~ matters
===

---

> Alice/Bob,

> I'd like to stop paying (company) to let the NSA read our email. If the NSA needs access to our email, they can snoop it at our ISP, or ask us directly.

> Would you take the extra steps to turn one of the servers into a full POP/IMAP server?

> Thanks,

> Jan

---

Notes:

You'll need a bridged network connection to really test this. Treat it as the external/public IP.

Configure your “DMZ/external/public” IP as 'yourname.sofree.us' and 'mail.yourname.sofree.us' in your hosts file, and at least one test client.

Always firewall an external connection appropriately, and consider firewalling internal interfaces whenever there's a reasonable chance that internal monkeys will be curious (or even malicious).