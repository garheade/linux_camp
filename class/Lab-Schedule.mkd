Lab: Scheduling routine tasks, like backups
===

---

> Alice/Bob,

> Files keep disappearing from my home directory! I was working on performance reports for the contractors and they're *gone*. We need to make sure that we're backing up home folders, at least!

> And, could you setup a job to capture the screen of every workstation every five minutes?

> --Jan, going crazy here.

> P.S. Make ~sure~ you send a log message every time a backup happens; I like to use logger for that. And capture the output to date-named log-files and/or email.

> P.P.S. I'm also concerned about /shared/. I want to move it to DRBD, TahoeLAFS, Gluster, or ceph. Someday, maybe. For now, just backup /home/.

---

Notes:

We want to use crontab, tar, rsync, and logger in the solution, at least.

Setup w1 first.

I'll just write a script to do a tar backup of /etc/ and an rsync backup of /home/ into /shared/backups/$( hostname ), but I'm going to lose all the file-ownership info. Not perfect, but good enough. A link to the script in /etc/cron.hourly will ensure that a test comes up soon.

If there's time, setup a cron-job to take a screenshot every five minutes. The hard part will be making that happen automatically, no matter who is logged in.

If there's even more time, complete the backup solution by ensuring that nothing important (/home/, /etc/, /shared/) on any server is "single instance".

If you ever have to do this in real life:

* Look where the time goes. Back up the things people spend the most time on, first and most frequently.
* Ensure that you have more than one copy of *anything* that matters, as far apart as possible.
* Educate your users that anything they care about should be verified to be backed up. Like security, data integrity is best regarded as *everyone's* job.
