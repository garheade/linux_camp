#SFS Linux Camp Class Materials

*Linux Camp, by David L. Willson and the Software Freedom School, copyright license CC BY SA 3.0*

Linux Camp 2014 Staff
---

David Willson, Heather Willson, Troy Ridgley, Gary Romero and Ross Brunson (LPI proctor).

Linux Camp
---

[Introductions](Intro.mkd)

[Pre-Lab](Pre-Lab.mkd)

[Lab-Build-W1.mkd](Lab-Build-W1.mkd)

[Lab-Build-S1.mkd](Lab-Build-S1.mkd)

[Lab-Build-S2.mkd](Lab-Build-S2.mkd)

[Lab-HardwareInventory.mkd](Lab-HardwareInventory.mkd)

[Lab-StorageInventory.mkd](Lab-StorageInventory.mkd)

[Lab-Users.mkd](Lab-Users.mkd)

[Lab-Root.mkd](Lab-Root.mkd)

[Lab-Sudoer.mkd](Lab-Sudoer.mkd)

[Lab-SSH.mkd](Lab-SSH.mkd)

[Lab-Network.mkd](Lab-Network.mkd)

[Lab-NFS.mkd](Lab-NFS.mkd)

[Lab-Doc-Share.mkd](Lab-Doc-Share.mkd)

[Lab-Scripts.mkd](Lab-Scripts.mkd)

[Lab-Mail.mkd](Lab-Mail.mkd)

[Lab-Later.mkd](Lab-Later.mkd)

[Lab-Schedule.mkd](Lab-Schedule.mkd)

[Lab-Evil-Eve.mkd](Lab-Evil-Eve.mkd)

[Lab-GUI.mkd](Lab-GUI.mkd)

[Lab-NTP.mkd](Lab-NTP.mkd)

[Lab-Print.mkd](Lab-Print.mkd)

[Lab-Recover.mkd](Lab-Recover.mkd)

As Time Allows
---

[Lab-Mail-O.mkd](Lab-Mail-O.mkd)

[Lab-Home.mkd](Lab-Home.mkd)
Kill this exercise! Don't ever do this, even if you have time! It is a Bad Idea!

[Lab-Web-O.mkd](Lab-Web-O.mkd)

[Lab-Wordpress-O.mkd](Lab-Wordpress-O.mkd)

[Lab-cdir-O.mkd](Lab-cdir-O.mkd)

[Lab-Alien-O.mkd](Lab-Alien-O.mkd)

[Lab-web-db-O.mkd](Lab-web-db-O.mkd)

[Lab-Shell-O.mkd](Lab-Shell-O.mkd)