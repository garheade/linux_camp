Good morning, everyone!

The Software Freedom School is excited to announce that registration for the 2014 Linux Camp will open on Friday, June 13th!

Linux Camp will be held from August 25th through the 29th at the beautiful YMCA Snow Mountain Ranch in Granby, CO. Each of the first 4 days will include ~10 hours of hands-on labs and instruction, 3 square meals made by the fabulous Heather Willson, breaks/evening where you can enjoy the amazing Rocky Mountains just outside the front door, along with a bed in the YMCA's wonderful cabin. On the 5th day, you can stay on site and take the LPI-101 and LPI-102 paper exams or, at your option, we'll provide you with a voucher to take the LPI exam of your choice at a later date. Ideally, every student will come to Linux Camp with about 1,000 hours of Linux Administration experience but those students that have less can attend our LPIC-1 Study Group to come up to speed. The LPIC-1 Study Group will take place over the 8 weeks leading up to Linux Camp and be free to registered Linux Campers. The value of this event far outweighs the cost, which is by far, the least expensive event of its kind.

Price is as follows:

Base Price ............... $2434

Early Bird Discount ...... -$150
Register before Independence Day

Early PIF Discount ....... -$150
Pay in full by Monday, 8/4

BYOB Discount ............ -$150
Bring your own machine to Camp

All discounts are stack-able so if you register before Independence Day, pay your tuition by August 4th, and bring your own computer, you can bring the price down to $1,984.

Full information on what Linux camp is available on our website: http://www.sofree.us/?p=112227
For information on the LPIC-1 Study Group: http://www.sofree.us/?p=151587
