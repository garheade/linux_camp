#!/bin/bash -eu
#Jeff White
#Create users and group
users="bob alice charlie dani eve fred jan"
groups="contractors linux_admins"
contractors="dani eve fred"
linux_admins="bob alice jan eve"
group_guid=10000
user_guid=9000

for group_name in $groups; do
	groupadd -g $group_guid $group_name
	group_guid=$(($group_guid+1000))
done

for user_name in $users; do
	groupadd -g $user_guid $user_name
	useradd -m -g $user_guid -u $user_guid $user_name
	chage -E 2013-08-24 $user_name
	user_guid=$(($user_guid+1))
done

for users_name in $contractors; do
	usermod -a -G contractors $users_name
done

for users_name in $linux_admins; do
	usermod -a -G linux_admins $users_name
done
