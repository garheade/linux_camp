**** Host Info ****
Microknoppix
Linux #25 SMP PREEMPT Sat Jun 15 15:27:01 CEST 2013
**** CPU / Memory ****
model name	: Intel(R) Core(TM) i5-3360M CPU @ 2.80GHz
model name	: Intel(R) Core(TM) i5-3360M CPU @ 2.80GHz
model name	: Intel(R) Core(TM) i5-3360M CPU @ 2.80GHz
model name	: Intel(R) Core(TM) i5-3360M CPU @ 2.80GHz
MemTotal:        3334804 kB
**** Storage Info ****
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 238.5G  0 disk 
├─sda1   8:1    0   350M  0 part 
└─sda2   8:2    0 238.1G  0 part 
sdb      8:16   1   3.8G  0 disk 
└─sdb1   8:17   1   3.8G  0 part /mnt-system
sr0     11:0    1  1024M  0 rom  
zram0  251:0    0   2.4G  0 disk [SWAP]
cloop0 240:0    0   1.9G  1 disk /KNOPPIX
**** Network Info ****
00:19.0 Ethernet controller: Intel Corporation 82579LM Gigabit Network Connection (rev 04)
**** Network Interface Info ***
eth0      Link encap:Ethernet  HWaddr 8c:c1:21:4c:e2:ae  
lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
wlan0     Link encap:Ethernet  HWaddr c8:f7:33:e2:82:36  
          inet addr:192.168.73.114  Bcast:192.168.73.255  Mask:255.255.255.0
          inet6 addr: fe80::caf7:33ff:fee2:8236/64 Scope:Link
**** VGA Info ****
01:00.0 VGA compatible controller: Advanced Micro Devices [AMD] nee ATI Cape Verde [Radeon HD 7700M Series]
